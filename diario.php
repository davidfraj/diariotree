<h1>Diario de clase <small>(Usar CTRL+F para buscar)</small></h1>

<?php  
//Creamos un array de los meses
$meses=array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');

//Establecemos el directorio
$directorio='.';
//Abrimos el directorio
$d=opendir($directorio);
//Volcamos los nombres de los directorios en un vector, para poder mostrarlos en orden 
$dias=array();
while($e=readdir($d)){
	if(is_dir($directorio.'/'.$e)){
		if(substr($e,0,3)=='201'){
			$dias[]=$e;
		}
	}
}
//Cerramos el directorio
closedir($d);

//Ordenamos dicho vector
sort($dias);

//Recorremos el vector ya ordenado
for ($i=0; $i < count($dias); $i++) { 
	
	//Extraemos info de la fecha
	$dia=substr($dias[$i],6,2);
	$mes=intval(substr($dias[$i],4,2));
	$anyo=substr($dias[$i],0,4);
	$diasem=substr($dias[$i],9);
	$mesnombre=$meses[$mes];

	//Mostramos el titulo
	echo '<h2>'.$diasem.', '.$dia.' de '.$mesnombre.' de '.$anyo.'</h2>';
	//Mostramos el contenido integro
	echo '<pre>';
	$diario=file_get_contents($directorio.'/'.$dias[$i].'/diario.txt');
	echo $diario;
	echo '</pre>';
	//Creamos un enlace a su comprimido en formato rar
	echo '<a href="'.$directorio.'/'.$dias[$i].'.rar">Descargar '.$dias[$i].'.rar</a>';
	echo '<hr>';

}

?>