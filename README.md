## VERSION
0.2

## CREACION
29 de Marzo de 2017

## Ultima ACTUALIZACION
29 de Marzo de 2017

## DESCRIPCION y FUNCIONAMIENTO
Se trata de un script rapido, para poder escanear todos los directorios almacenados con el formato A�OMESDIA_DIASEMANA, siendo el a�o en formato de 4 digitos, el dia en formato de 2 digitos, el mes de 2 digitos y el dia de la semana, mediante el nombre del dia.
Aqui tenemos algun ejemplo de directorio:

20170320_lunes
20170324_jueves
20170327_lunes

El archivo diario.php, se tiene que situar en el mismo directorio que dichos directorios, quedando por ejemplo asi.

/directorio/20170320_lunes
/directorio/20170324_jueves
/directorio/20170327_lunes
/directorio/diario.php

Dentro de cada directorio de dia de clase, deberiamos tener un fichero llamado diario.txt, que es el que contendra el texto que aparecera en la pagina web



